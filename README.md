
<!-- README.md is generated from README.Rmd. Please edit that file -->

# geohealth

<!-- badges: start -->

[![Project Status: WIP – Initial development is in progress, but there
has not yet been a stable, usable release suitable for the
public.](https://www.repostatus.org/badges/latest/wip.svg)](https://www.repostatus.org/#wip)
[![HAL](https://img.shields.io/badge/Based%20on-hal--01787435-brightgreen)](https://hal.archives-ouvertes.fr/hal-01787435)
<!-- badges: end -->

## Installation

You can install the development version of geohealth like so:

``` r
# install.packages(remotes)
remotes::install_git("https://framagit.org/espace-dev/geohealth/geohealth")
library(geohealth)
```

## Package dependencies

![](man/figures/dependency_heatmap.png)
