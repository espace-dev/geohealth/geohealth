---
output: github_document
---

<!-- README.md is generated from README.Rmd. Please edit that file -->

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  fig.path = "man/figures/README-",
  out.width = "100%"
)
```

# geohealth

<!-- badges: start -->

[![Project Status: WIP – Initial development is in progress, but there has not yet been a stable, usable release suitable for the public.](https://www.repostatus.org/badges/latest/wip.svg)](https://www.repostatus.org/#wip)
[![HAL](https://img.shields.io/badge/Based%20on-hal--01787435-brightgreen)](https://hal.archives-ouvertes.fr/hal-01787435)
<!-- badges: end -->

## Installation

You can install the development version of geohealth like so:

```{r eval=FALSE}
# install.packages(remotes)
remotes::install_git("https://framagit.org/espace-dev/geohealth/geohealth")
library(geohealth)
```

## Package dependencies

```{r echo=FALSE, message=FALSE, warning=FALSE}
quiet <- function(x) { 
  sink(tempfile()) 
  on.exit(sink()) 
  invisible(force(x)) 
} 
suppressPackageStartupMessages(library(pkgndep))
pkg <- quiet(pkgndep(getwd(), verbose = FALSE))
dependency_heatmap(pkg, file = "man/figures/dependency_heatmap.png")
```

![](man/figures/dependency_heatmap.png)
